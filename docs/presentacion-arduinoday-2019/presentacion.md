<!-- $theme: default -->
<!-- $size: 16:9 -->
<!-- page_number: true -->

<center>

# Star Tracker DIY con ESP8266 y Arduino IDE

Oliver Gutiérrez Suárez

</center>

---

<center>

# ¿Quién es este tío?
![Doc](img/yo.jpg)
Desarrollador de software libre aficionado a la astrofotografía

</center>

---

<center>

# Astrofotografía
![M101](img/m101.jpg)
Sacar fotos de "modelos" en el infinito
Poca luz = tiempos de exposición largos
Muchas fotos y luego "sumarlas"

</center>

---

<center>

# Descripción del problema
![Circumpolar](img/circumpolar.jpg)
Movimiento del cielo
¡Seguimiento!

</center>

---

<center>

# Solución 1
![Plancheta](img/plancheta.jpg)
Plancheta de bisagra "de toda la vida"

</center>

---

<center>

# Solución 2
![Doc](img/doc.jpg)
Inventarme algo con lo que pudiera encontrar

</center>

---

<center>

# Plancheta de bisagra "de toda la vida"
Voluminoso y pesado
Manual


</center>

---

<center>

# Inventar algo
Más interesante
Usar electrónica para automatizar el movimiento

</center>

---

<center>


# Segunda opción
![Approved](img/approved.jpg)


</center>

---

<center>

# Idea: Sistema tipo gato de tijera

![Briza](img/briza.jpg)
(Ningún animal fue dañado durante este experimento)

</center>

---

<center>

# Gato de tijera

![Gato de tijera](img/gato-tijera.jpg)
El tornillo junta ambas bisagras elevando el conjunto


</center>

---


<center>

# Busqueda de componentes
![Explorador](img/explorador.jpg)
Expedición al chino de la esquina

</center>

---

<center>

# Armazón
![Bisagra](img/bisagra.jpg)
Bisagra de pared para mesa plegable
</center>

---

<center>

## Parte electrónica
NodeMCU: Básicamente un ESP8266 (¡WiFi!)
Controlador de motor
Motor paso a paso

</center>

---

<center>

# Algunos de los componentes
![Existencias](img/existencias.jpg)

</center>

---

<center>

# Diagrama básico
![Diagrama bisagra](img/diagrama-bisagra.jpg)


</center>

---

<center>

# Y usando un poco de trigonometría...
![Diagrama trigonometria](img/diagrama-trigonometria.jpg)

</center>

---

<center>

# Esquema de la electrónica
![Esquema electrónica](img/esquema-electronica.png)

</center>

---



<center>

# Preparar Arduino IDE para ESP8266
![Plugin ESP8266](img/esp8266plugin.jpg)

Más información: https://github.com/esp8266/Arduino


</center>

---

<center>

# Diferencias con Arduino

</center>

```
// NodeMCU pin mapping
// -------------------
static const uint8_t D0   = 16;
static const uint8_t D1   = 5;
static const uint8_t D2   = 4;
static const uint8_t D3   = 0;
static const uint8_t D4   = 2;
static const uint8_t D5   = 14;
static const uint8_t D6   = 12;
static const uint8_t D7   = 13;
static const uint8_t D8   = 15;
static const uint8_t D9   = 3;
static const uint8_t D10  = 1;

```

---

<center>

# Cálculos

</center>


```
void update_angle() {
  currentAngle = INITIAL_ALPHA + ALPHA_PER_SECOND * elapsedTime;
}


double calculate_base_distance() {
  double squared_height = pow(FIXED_SHORT_SIDE * sin(currentAngle), 2);
  return sqrt(SQUARED_SHORT_SIDE - squared_height)
         + sqrt(SQUARED_LONG_SIDE - squared_height);
}


int calculate_needed_steps(double d1, double d2) {
  return floor((d1 - d2) / DISTANCE_PER_STEP);
}
```

---

<center>

# Programa principal (loop)

</center>

```
// Current time
double now = (millis() / 1000.0) - startTime;
  
if (now - elapsedTime > 0.5) {
  elapsedTime = now; 
    
  // Update angle
  update_angle();
    
  // Calculate base distance
  double base = calculate_base_distance();

  // Calculate needed steps to increment to new distance
  double steps = calculate_needed_steps(currentDistance, base);

  if (steps > 0) {
    motor.step(steps);
    stepCount += steps;
    currentDistance -= steps * DISTANCE_PER_STEP;
  }
}
```

---

<center>

# ¿Y ahora como controlo yo esto?

</center>

```
void connect_to_wifi(ssid, password) {
  // Connect
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
}
```

<center>

![Interfaz web](img/interfaz-web.jpg)
Interfaz web básico por WiFi

</center>

---

<center>

# Resultado Final
![R3 Star Tracker](img/r3startracker.jpg)

</center>

---

<center>

# Resultados
![Interfaz web](img/resultado1.jpg) ![Interfaz web](img/resultado2.jpg)
Izquierda: 120 segundos exposición única (se nota algo de movimiento)
Derecha: Suma de varias exposiciones de 30 segundos y procesado

</center>

---

<center>

# Mejoras I
![Mejora ajuste](img/mejora-ajuste.jpg)
Mejorar el ajuste de distintas piezas (menos juego)

</center>

---

<center>

# Mejoras II
![Mejora DHT22](img/mejora-dht22.jpg)
Añadir DHT22 para leer temperatura y humedad

</center>

---

<center>

# GRACIAS
A **ustedes** por asistir
A **Eugenio** por invitarme a venir
A **Víctor** por engancharme a la astrofotografía
A **Luis** y a **muchos más** por responder a mis preguntas

</center>

---

<center>

# Oliver Gutiérrez Suárez
**Web**: https://starlighthunter.com
**Código en Gitlab**: https://gitlab.com/olivergs/r3-star-tracker

</center>

