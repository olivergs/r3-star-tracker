
/*
 Star tracker program
 Oliver Gutiérrez <ogutsua@gmail.com>

 NodeMCU pin mapping
 -------------------
  
 static const uint8_t D0   = 16;
 static const uint8_t D1   = 5;
 static const uint8_t D2   = 4;
 static const uint8_t D3   = 0;
 static const uint8_t D4   = 2;
 static const uint8_t D5   = 14;
 static const uint8_t D6   = 12;
 static const uint8_t D7   = 13;
 static const uint8_t D8   = 15;
 static const uint8_t D9   = 3;
 static const uint8_t D10  = 1;

*/

#include <Stepper.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <DHT.h>

// WiFi Configuration
const char *ssid = "R3";
const char *password = "";

// Astronomic day: 23h 56m 4s = 86164s
const int SECONDS_ASTRO_DAY = 86164;

// Angle information in radians
const double INITIAL_ALPHA = 0.633119;
const double ALPHA_PER_SECOND = (2.0 * PI) / SECONDS_ASTRO_DAY;

// Distance information
const double INITIAL_DISTANCE = 163; // 163mm
const double FIXED_LONG_SIDE = 120; // 120mm
const double FIXED_SHORT_SIDE = 60; // 60mm
// Powers of 2 for easy calculation
const double SQUARED_LONG_SIDE = pow(FIXED_LONG_SIDE, 2);
const double SQUARED_SHORT_SIDE = pow(FIXED_SHORT_SIDE, 2);

// Motor information
const int STEPS_PER_REVOLUTION = 200;
const double DISTANCE_PER_REVOLUTION = 1; // 1mm
const double DISTANCE_PER_STEP = DISTANCE_PER_REVOLUTION / STEPS_PER_REVOLUTION; 

// Temperature constants
const int TEMP_DELAY = 10000; // Temperature delay in ms


// Website template
const char *web_template = "<html>\
  <head>\
    %s\
    <meta charset=\"utf-8\">\
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\
    <title>R3 Star Tracker v0.1</title>\
    <style>\
      body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }\
    </style>\
  </head>\
  <body>\
    <h1>R3 Star Tracker v0.1</h1>\
    <div>%s</div>\
  </body>\
</html>";


// Initialize stepper on pins D5 to D8:
Stepper motor(STEPS_PER_REVOLUTION, D5, D6, D7, D8);

// Initialize DHT sensor
DHT dht(D4, DHT22); 

// Web server initialization
ESP8266WebServer server(80);


// Status variables
double elapsedTime = 0;       // Time passed since start
double startTime = 0;       // Start time
int stepCount = 0;         // Steps moved since start
double currentAngle = INITIAL_ALPHA;
double currentDistance = INITIAL_DISTANCE;
bool working = false;
double tempTime = 0;
float humidity = 0;
float temperature = 0;


void connect_to_wifi() {
  Serial.println("");
  Serial.print("Connecting to SSID ");
  Serial.print(ssid);
  Serial.print(": ");
  // Connect
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}


void setup_webserver() {
  server.on("/", handleRoot);
  server.on("/start/", start_tracking);
  server.on("/stop/", stop_tracking);
  server.on("/reset/", reset_tracking_status);
  server.on("/status/", tracking_status);
  server.onNotFound(handleNotFound);
  server.begin();
  Serial.println("HTTP server started");
}


void reset_status() {
  Serial.println("---- Resetting status ----");
  elapsedTime = 0;
  stepCount = 0;
  currentAngle = INITIAL_ALPHA;
  currentDistance = INITIAL_DISTANCE;
}


void reset_tracking_status() {
  reset_status();
  server_send_html(200, "<p>Resetted</p><div><a href=\"/\">Back</a></div>", false);
}


void start_tracking() {
  Serial.println("---- Starting ----");
  reset_status();
  startTime = millis() / 1000;
  working = true;
  server_send_html(200, "<p>Tracking started</p><div><a href=\"/\">Back</a></div>", false);
}


void stop_tracking() {
  working = false;
  Serial.println("---- Stopped ----");
  print_status();
  Serial.println("-----------------");
  server_send_html(200, "<p>Tracking stopped</p><div><a href=\"/\">Back</a></div>", false);
}

void tracking_status() {
  String statusdata = "";
  statusdata += "* Elapsed time: ";
  statusdata += elapsedTime;
  statusdata += "\n* Rotation angle: ";
  statusdata += currentAngle;
  statusdata += "\n* Total steps: ";
  statusdata += stepCount;
  statusdata += "\n* Base distance: ";
  statusdata += currentDistance;
  statusdata += "\n* Temperature: ";
  statusdata += temperature;
  statusdata += "\n* Humidity: ";
  statusdata += humidity;
  server.send(200, "text/plain", statusdata);
}


void update_angle() {
  currentAngle = INITIAL_ALPHA + ALPHA_PER_SECOND * elapsedTime;
}


double calculate_base_distance() {
  double squared_height = pow(FIXED_SHORT_SIDE * sin(currentAngle), 2);
  return sqrt(SQUARED_SHORT_SIDE - squared_height)
         + sqrt(SQUARED_LONG_SIDE - squared_height);
}


int calculate_needed_steps(double d1, double d2) {
  Serial.print("Base difference: ");
  Serial.println(d1 - d2, 10);
  return floor((d1 - d2) / DISTANCE_PER_STEP);
}


void print_status() {
  char var[50];
  Serial.print("Elapsed time: ");
  Serial.println(elapsedTime, 10);
  Serial.print("Rotation angle: ");
  Serial.println(currentAngle, 10);
  Serial.print("Total steps: ");
  Serial.println(stepCount);
  Serial.print("Base distance: ");
  Serial.println(currentDistance, 10);
  Serial.print("Temperature: ");
  Serial.println(temperature, 10);
  Serial.print("Humidity: ");
  Serial.println(humidity, 10);
}


void server_send_html(int code, char* content, bool refresh) {
  char temp[2000];
  if(refresh) {
      snprintf(temp, 2000, web_template, "<meta http-equiv=\"refresh\" content=\"5\">", content);
  } else {
      snprintf(temp, 2000, web_template, "", content);
  }
  server.send(code, "text/html", temp);
}

void handleRoot() {
  char content[400];
  int sec = millis() / 1000;
  int min = sec / 60;
  int hr = min / 60;
  snprintf(content, 200, "\
    <p>Uptime: %02d:%02d:%02d</p>\
    <div>\
      <iframe src=\"/status/\" width=\"100%s\" height=\"200\"></iframe>\
    </div>\
    <div>\
      <a href=\"/start/\">Start</a> \
      <a href=\"/stop/\">Stop</a> \
      <a href=\"/reset/\">Reset</a>\
    </div>\
    ", hr, min % 60, sec % 60, "%");
  server_send_html(400, content, true);
}


void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";

  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }

  server.send(404, "text/plain", message);
}

void read_dht22() {
  Serial.println("Reading from DHT sensor");
  humidity = dht.readHumidity();
  temperature = dht.readTemperature();
  if (isnan(humidity) || isnan(temperature)) {
    Serial.println("Failed to read from DHT sensor!");
  }
}

void setup() {
  Serial.println("---- Initializing ----");
  // Initialize the serial port:
  Serial.begin(9600);
  // Initialize DHT sensor
  dht.begin();
  connect_to_wifi();
  setup_webserver();
}


void loop() {
  // Handling server clients
  server.handleClient();
  // Moving steppers if needed
  if (working) {
    // Increment current time
    double now = (millis() / 1000.0) - startTime;
    if (now - elapsedTime > 0.5) {
      elapsedTime = now;
      // Update angle
      update_angle();
      // Calculate base distance
      double base = calculate_base_distance();
      Serial.print("* Calculated base distance: ");
      Serial.println(base);
      // Calculate needed steps to increment to new distance
      double steps = calculate_needed_steps(currentDistance, base);
      Serial.print("* Steps to move: ");
      Serial.println(steps);
      print_status();
      if (steps > 0) {
        // Move the motor
        motor.step(steps);
        Serial.print("Moved: ");
        Serial.print(steps);
        Serial.print("steps. Distance: ");
        Serial.println(currentDistance - base);
        // Update status
        stepCount += steps;
        currentDistance -= steps * DISTANCE_PER_STEP;
      } else {
        Serial.print("No steps to move. Distance: ");
        Serial.println(currentDistance - base);
      }
    }
  }
  if ((millis() - tempTime) >= TEMP_DELAY) {
    read_dht22();
    tempTime = millis();
    Serial.print("Temperature: ");
    Serial.println(temperature, 2);
    Serial.print("Humidity: ");
    Serial.println(humidity, 2);
  }
}

